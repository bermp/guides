# Virtual Machines

`$ sudo apt install qemu-kvm qemu virt-manager virt-viewer`

`$ sudo apt install open-vm-tools`

https://www.qemu.org

https://xenproject.org

https://anbox.io

https://www.bluestacks.com

https://www.genymotion.com


## VMware

https://www.vmware.com/products/workstation-pro.html

https://www.vmware.com/products/fusion.html

`$ sudo vmware-installer -u vmware-workstation`

`$ sudo vmware-modconfig --console --install-all`

https://communities.vmware.com/t5/VMware-Workstation-Pro/Solution-for-Hardware-graphics-acceleration-and-No-3D-support/td-p/508060

* Press ctrl + alt to click out of vm window and navigate host OS

## Virtualbox

https://www.virtualbox.org/wiki/Downloads

`$ sudo /sbin/vboxconfig`

Download OS
* Download the live cd/installation media for the OS
* For Linux distro info: http://distrowatch.org
* Virtualbox Supported Formats
	* .iso, .dmg, .cdr, .cue, .viso
	* VDI  – Virtual Disk Image
	* VMDK – VMware virtual hard disk format
	* VHD  – Microsoft virtual hard disk format
	* HDD  – Parallels Version 2 format

Create Virtual Machine
* Launch VirtualBox App
* On the top right, click 'New'
* Give the machine a name and select the type
	* The app is smart enough that if you name the machine 'arch' 'centos' windows' 'osx', etc it will auto populate the type and machine

Configure Machine
* When the machine is created it will show up on the left hand side of the VirtualBox app
* Select the virtual machine, then click settings
* Under 'System' make sure Optical is before Hard Disk for boot order
* Under 'Storage' select the empty optical drive
* On the farthest right side click on the small, blue disc image
* Navigate to the downloaded OS image file
* Save all settings
* Start the virtual machine
	* On the first boot, the machine will use the downloaded image file as the boot drive
	* Install the OS onto the hard drive of virtual machine

* Press right ctrl key + left click to click out of vm window and navigate host OS


## Hyper-V
Turn on Hyper-V
Start --> Windows Features
Check the Hyper-V box on
Reboot machine

Configure VM
Start --> Hyper-V Manager
One the left side , right click on the name of the machine then click on 'Quick Create'
Install OS from downloaded .iso

To configure network
* Type “nmcli d” command in your terminal for quick list ethernet card installed on your machine
* Type “nmtui” command in your terminal to open Network manager. After opening Network manager chose “Edit connection” and press Enter (Use TAB button for choosing options)
* Choose you network interfaces and click 'Edit'
* DHCP: Choose “Automatic” in IPv4 CONFIGURATION and check Automatically connect check box and press OK and quit from Network manager
* Reset network services > service network restart
