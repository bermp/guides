# Development Environment Config

Editors and IDEs
* TextMate https://macromates.com
* SublimeText https://www.sublimetext.com/3
* VS Codium https://github.com/VSCodium/vscodium/releases
    * https://gitduck.com
* JetBrains https://www.jetbrains.com
* NetBeans https://netbeans.org
* Cloud9 https://aws.amazon.com/cloud9
* Atom https://atom.io

Terminal Emulators and Command-line Interpreters
* https://github.com/tmux/tmux/wiki
* https://gnometerminator.blogspot.com/p/introduction.html
* https://gnunn1.github.io/tilix-web
* https://iterm2.com
* https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
* https://github.com/Swordfish90/cool-retro-term
* https://conemu.github.io
* https://cmder.net


### Debian
`$ sudo apt install gcc g++ llvm make build-essential apt-transport-https`


Sublime Text

`$ wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -`

`$ echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list`

`$ sudo apt update && sudo apt install sublime-text`


Git

```
$ sudo apt install git
$ git config --global user.name "your_username"
$ git config --global user.email "your_email_address@example.com"
$ git config --global --list
$ ssh-keygen -t ed25519 -C "<comment>"
$ ssh-keygen -t rsa -b 2048 -C "email@example.com"
$ ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
```


Python pip https://pip.pypa.io/en/stable/installing

`$ sudo apt install python-is-python3 python3-pip python3-setuptools`

virtualenv https://virtualenv.pypa.io/en/latest/installation.html

Anaconda https://www.anaconda.com/products/individual

`$ sudo apt install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6`

`bash ~/Downloads/Anaconda3-YYYY.MM-Linux-x86_64.sh` OR `./Anaconda3-YYYY.MM-Linux-x86_64.sh`


Rust curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
cargo install --locked cargo-outdated
rustup self uninstall


ReactJS https://nodejs.org/en/download/

```
$ curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
$ sudo apt install -y nodejs
$ npx create-react-app my-app` OR `npm init react-app my-app`
$ cd my-app
$ npm start
```


Flutter https://flutter.dev/docs/get-started/install/linux and https://dart.dev/get-dart


Extra

`$ sudo apt install bleachbit byobu exfat-utils exfat-fuse fonts-crosextra-{carlito,caladea} gpa hfsutils hfsprogs lm-sensors ntfs-3g openvpn tmux ttf-mscorefonts-installer xz-utils`

https://zealdocs.org

https://www.tensorflow.org/install

https://rstudio.com/products/rstudio/download

https://amdgpu-install.readthedocs.io/en/latest/install-installing.html

https://docs.openshift.com

https://kubernetes.io/docs/setup/production-environment/tools

https://kubernetes.io/docs/tasks/tools/install-kubectl


### Darwin
`xcode-select --install`

Homebrew https://brew.sh
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
$ brew tap gmerlino/pciutils
$ brew install --HEAD gmerlino/pciutils/
$ brew update && brew install bash
```

https://zipzapmac.com/Go2Shell
https://github.com/Breathleas/Go2Shell

https://kapeli.com/dash


### NT
https://git-scm.com/download

https://www.python.org/downloads

https://nodejs.org/en/download

Add Sublime Text to PATH
* System Properties -> Advanced System Settings -> Advanced -> Environment Variables
    * Shortcut is to open command line and type `sysdm.cpl`
    * Create a new system variable called SUBLIME that will point to the folder of your Sublime installation
        * To add the System Variable to PATH, Add the following to the end of PATH variable: ;%SUBLIME%
        * New commands
            * `subl.exe` and `subl` both launch app
            * `subl .` opens current folder within app


### Checks
* bash --version
* git --version
* python --version
* python3 --version
* pip --version
* pip3 --version
* rustc --version
* cargo --version
* npm -v
* node -v
* ruby -v
* rails -v
* rbenv -v
* heroku -v


### Gitignore
Bash
`git config --global core.excludesfile ~/.gitignore`


cmd.exe
`git config --global core.excludesfile %USERPROFILE%\.gitignore`
`git config --global core.editor "C:/Program Files/Sublime Text 3/sublime_text.exe"`


.gitignore_global file contents

    #compiled source
    *.com
    *.class
    *.dll
    *.exe
    *.o
    *.so
     
    # Packages
    *.7z
    *.dmg
    *.gz
    *.iso
    *.jar
    *.rar
    *.tar
    *.zip
     
    # Logs and databases
    *.log
    log/*
    *.sql
    *.sqlite
    *.sqlite3
    *.sqlite3-journal
    
    # WebStorm
    .idea/

    # OS generated files
    .DS_Store
    .DS_Store?
    ._*
    *.app
    .Spotlight-V100
    .Trashes
    ehthumbs.db
    Thumbs.db
    *.excludes
    *.json
     
    # codekit
    .sass-cache/
    .codekit-config.json
    config.codekit
    
    # Others
    _pycache_
    *.xcf
    *.autosave
    npm-debug.log

    # C++ objects and libs
    *.slo
    *.lo
    *.o
    *.a
    *.la
    *.lai
    *.so
    *.dll
    *.dylib

    # Qt-es
    /.qmake.cache
    /.qmake.stash
    *.pro.user
    *.pro.user.*
    *.moc
    moc_*.cpp
    qrc_*.cpp
    ui_*.h
    Makefile*
    *-build-*
    
    # vi
    *~
