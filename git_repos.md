# Git Set-up and Maintenance

### Git Version Control Tool
Usually already installed on many Linux distros and macOS
https://git-scm.com/download

### Git Repositories
https://gitlab.com
https://github.com
https://bitbucket.org

### Create SSH key pair
https://docs.gitlab.com/ee/ssh
https://help.github.com/en/articles/connecting-to-github-with-ssh

### Set Identity
```
$ sudo apt install git
$ git config --global user.name "your_username"
$ git config --global user.email "your_email_address@example.com"
$ git config --global --list
$ ssh-keygen -t ed25519 -C "<comment>"
$ ssh-keygen -t rsa -b 2048 -C "email@example.com"
$ ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
```

Can use public or private email
https://docs.gitlab.com/ee/user/profile/index.html#change-the-email-displayed-on-your-commits
https://github.com/settings/emails

### Create and Manipulate Repository
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
https://docs.gitlab.com/ee/user/project/repository
https://help.github.com/en/articles/create-a-repo
https://help.github.com/en/articles/adding-a-remote
https://help.github.com/en/articles/which-remote-url-should-i-use

### Managing Multiple Identities
Create new ssh key with a new name (examples below)
* id_rsa_a and id_rsa_b
* work_rsa and personal_rsa

Configure an individual repo to use a specific user / email address which overrides the global configuration. From the root of the repo run
```
git config user.name "Your Name Here"
git config user.email your@email.com
```

Create or modify config file
```
$ cd ~/.ssh/
$ touch config
$ subl -a config
```

Sample config file contents
```
#Personal GitHub account
Host github.com
 HostName github.com
 User git
 AddKeysToAgent yes
 UseKeychain yes
 IdentityFile ~/.ssh/id_rsa
 
#Work GitHub account
Host github.com-work
 HostName github.com
 User git
 AddKeysToAgent yes
 UseKeychain yes
 IdentityFile ~/.ssh/work_rsa
```

Use identity specified in config
* git clone git@github.com:personal/my_repo.git
* git clone git@github.com-work:[my work GitHub group]/[my project].git

### Markdown
* https://daringfireball.net/projects/markdown
* https://docs.gitlab.com/ee/user/markdown.html
* https://about.gitlab.com/handbook/markdown-guide
* https://help.github.com/en/articles/basic-writing-and-formatting-syntax
* https://guides.github.com/features/mastering-markdown
